defmodule DistributedEcs.NullSystem do
  @moduledoc false
  use DistributedEcs.System
  def aspect, do: %Aspect{}

  def dispatch(entity) do
    %Changes{}
  end
end
