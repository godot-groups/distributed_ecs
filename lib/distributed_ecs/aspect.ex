defmodule DistributedEcs.Aspect do
  defstruct with: [],
            without: []

  @type t :: %DistributedEcs.Aspect{
          with: [atom()],
          without: [atom()]
        }

  def new(with: with_components, without: without_components)
      when is_list(without_components)
      when is_list(with_components) do
    %DistributedEcs.Aspect{
      with: with_components,
      without: without_components
    }
  end
end
