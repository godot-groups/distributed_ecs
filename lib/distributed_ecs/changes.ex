defmodule DistributedEcs.Changes do
  defstruct attached: [],
            updated: [],
            removed: []

  @type attached_component :: atom() | DistributedEcs.Component.t()

  @type t :: %DistributedEcs.Changes{
          attached: [attached_component],
          updated: [DistributedEcs.Component.t()],
          removed: [atom()]
        }
end
