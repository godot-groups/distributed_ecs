defmodule DistributedEcs.Mixfile do
  use Mix.Project

  def project do
    [
      app: :distributed_ecs,
      version: "0.1.4",
      elixir: "~> 1.5",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      name: "DistributedEcs",
      deps: deps(),
      package: package(),
      docs: docs()
    ]
  end

  def application do
    [extra_applications: [:logger]]
  end

  defp package do
    [
      description: "A distributed ECS (Entity-Component-System) framework in Elixir",
      licenses: ["MIT"],
      maintainers: ["K. S. Ernest (iFire) Lee"],
      links: %{Gitlab: "https://gitlab.com/godot-groups/distributed_ecs.git"},
      source_url: "https://gitlab.com/godot-groups/distributed_ecs.git"
    ]
  end

  defp docs do
    [
      main: "DistributedEcs",
      source_url: "https://gitlab.com/godot-groups/distributed_ecs.git"
    ]
  end

  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:uuid, "~> 1.1"},
      {:gen_stage, "~> 0.12.2"}
    ]
  end
end
